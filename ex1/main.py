from tabulate import tabulate

file = open("log.txt", "r")

ip_list = []
link_list = []


def count(_list):
    _dict = {}
    for el in _list:
        if el in _dict:
            _dict[el] += 1
        else:
            _dict[el] = 1
    return _dict


for item in file:
    ip_list.append(item.split(' ')[0])
    if 'GET' in item:
        link_list.append(item.split('GET ')[1].split(' ')[0])
    if 'OPTIONS' in item:
        link_list.append(item.split('OPTIONS ')[1].split(' ')[0])


def make_table(_list, header):
    _dict = count(_list)
    if '\n' in _dict.keys():
        del _dict['\n']
    ip_result = sorted(_dict.items(), key=lambda x: x[1])[::-1]
    print(tabulate(ip_result, headers=header, tablefmt='pipe'))
    print()


make_table(ip_list, ('IP', 'NUM'))
make_table(link_list, ('LINK', 'NUM'))
